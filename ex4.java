package gr30125.Fleser.Marian.l3;

import becker.robots.*;

public class ex4 {
	public static void main(String[] args)
	{
		City ny = new City();
		Wall blockAve0 = new Wall(ny, 1, 0, Direction.WEST);
		Wall blockAve1 = new Wall(ny, 2, 0, Direction.WEST);
		Wall blockAve2 = new Wall(ny, 1, 0, Direction.NORTH);
		Wall blockAve3 = new Wall(ny, 1, 1, Direction.NORTH);
		Wall blockAve4 = new Wall(ny, 2, 0, Direction.SOUTH);
		Wall blockAve5 = new Wall(ny, 2, 1, Direction.SOUTH);
		Wall blockAve6 = new Wall(ny, 1, 1, Direction.EAST);
		Wall blockAve7 = new Wall(ny, 2, 1, Direction.EAST);
		Robot mark = new Robot(ny, 0, 2, Direction.WEST);
		
		
		mark.move();
		mark.move();
		mark.move();
		mark.turnLeft();
		mark.move();
		mark.move();
		mark.move();
		mark.turnLeft();
		mark.move();
		mark.move();
		mark.move();
		mark.turnLeft();
		mark.move();
		mark.move();
		mark.move();
		mark.turnLeft();
		
	}

}
