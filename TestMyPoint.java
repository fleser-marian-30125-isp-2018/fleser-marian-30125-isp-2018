package gr30125.Fleser.Marian.l3;

public class TestMyPoint {
	public static void main(String[] args) {
		MyPoint p1 = new MyPoint(); // Test constructor
		MyPoint p2 = new MyPoint();// Test constructor
		MyPoint p3 = new MyPoint(3, 4);// Test constructor
		System.out.println(p1);// Test toString()
		System.out.println(p2); //Test toString()
		System.out.println(p3);// Test toString()
		p2.set_x(1);   // Test setters 
		p2.set_y(1);  
		System.out.println(p2);
		System.out.println("x is: " + p2.get_x());  // Test getters
		System.out.println("y is: " + p2.get_y());
		p2.setXY(3, 0);   // Test setXY()
		System.out.println(p2);
		// Testing the overloaded methods distance()    
		System.out.println(p1.distance(1, 3));  
		System.out.println(p1.distance(p1));    
		System.out.println(p3.distance(p1));
	      

	}

}
