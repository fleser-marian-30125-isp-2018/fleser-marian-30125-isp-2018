package gr30125.Fleser.Marian.l3;

import becker.robots.*;

public class ex1 {
	public static void main(String[] args)
	{
		// Set up the initial situation
		City prague = new City();
		Thing parcel = new Thing(prague, 1, 2);
		  Robot karel = new Robot(prague, 1, 0, Direction.EAST);
		  
		  //Direct the robot to the final situation
		  karel.move();
		  karel.move();
		  karel.pickThing();
		  karel.move();
		  karel.turnLeft();
		  karel.turnLeft();
		  karel.turnLeft();
		  karel.move();
		  karel.putThing();
		  karel.move();
				  
	}

}